from datetime import datetime
import caldav
from caldav.elements import dav, cdav
import cherrypy
from cherrypy.lib.static import serve_file
import icalendar
import random
from os.path import basename, abspath, dirname, join
import uuid
from datetime import datetime
import ConfigParser
import getpass


config = ConfigParser.ConfigParser({'password': None})
config.read(['settings.ini.dist', 'settings.ini'])



url = config.get('CalDAV', 'url')
user = config.get('CalDAV', 'username')
password = config.get('CalDAV', 'password')

if password == None:
    appid = "python-task-stripboard %s" % url
    import keyring
    password = keyring.get_password(appid, user)
    if password == None:
        password = getpass.getpass("Please give password for CalDAV user %s: " % user).strip()
        keyring.set_password(appid, user, password)


def get_client():
    return caldav.DAVClient(url, username=user, password=password)

def get_calendar(client):
    principal = caldav.Principal(client)
    calendars = principal.calendars()
    return calendars[1]

def get_event_by_url(url, calendar):
    for e in calendar.events():
        if e.url == url:
            return e

def update_task_order(url, order, calendar = None):
    if None == calendar:
        c = get_client()
        calendar = get_calendar(c)
    e = get_event_by_url(url, calendar)
    e.load()
    cal = icalendar.Calendar.from_ical(e.get_data())
    for sub in cal.subcomponents:
        if type(sub) != icalendar.Todo:
            continue
        sub['X-ORDER'] = order
    e.data = cal.to_ical()
    e.save()

def remove_task(url):
    c = get_client()
    calendar = get_calendar(c)
    e = get_event_by_url(url, calendar)
    e.load()
    cal = icalendar.Calendar.from_ical(e.get_data())
    for sub in cal.subcomponents:
        if type(sub) != icalendar.Todo:
            continue
        sub['STATUS'] = 'COMPLETED'
    e.data = cal.to_ical()
    e.save()

def add_task(cal, uid):
    c = get_client()
    calendar = get_calendar(c)
    e = caldav.Event(c, data = cal.to_ical(), parent=calendar)
    e.id = uid
    e.save()


class JSONServer(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_tasks(self):
        data = []
        order_seen = []
        c = get_client()
        calendar = get_calendar(c)
        for e in calendar.events():
            e.load()
            cal = icalendar.Calendar.from_ical(e.get_data())
            for sub in cal.subcomponents:
                if type(sub) != icalendar.Todo:
                    continue
                if 'STATUS' in sub and sub['STATUS'] == 'COMPLETED':
                    continue
                order = float(sub['X-ORDER']) if 'X-ORDER' in sub else 0.0
                order_changed = False
                if order in order_seen:
                    order += random.random()
                    update_task_order(str(e.url), order, calendar)
                order_seen.append(order)
                data.append({
                    'url': str(e.url), 
                    'text': sub['SUMMARY'].to_ical(), 
                    'order': order
                })
        data.sort(key = lambda x: x['order'])
        return data

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def update_task_order(self):
        data = cherrypy.request.json
        url = data['url']
        order = "%f" % data['order']
        update_task_order(url, order)
        return "OK"

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def add_task(self):
        data = cherrypy.request.json
        
        cal = icalendar.Calendar()
        cal.add('VERSION', icalendar.vText('2.0'))
        cal.add('PRODID', icalendar.vText('//some strange python script by daniel'))
        cal.add('CREATED', datetime.now())
        cal.add('DTSTAMP', datetime.now())
        cal.add('LAST-MODIFIED', datetime.now())
        todo = icalendar.Todo()
        todo.add('SUMMARY', icalendar.vText(data['text']))
        todo.add('CLASS', 'PUBLIC')
        uid = str(uuid.uuid1())
        cal.add_component(todo)
        add_task(cal, uid)
        return "OK"

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def remove_task(self):
        data = cherrypy.request.json
        url = data['url']
        remove_task(url)
        return "OK"



root = dirname(abspath(__file__))
indexfile = join(root, 'index.html')
iconfile = join(root, 'icon_128.png')

conf = {
    '/': {
        'tools.caching.on': False,
        },
    '/index.html': {
        'tools.staticfile.on': True,
        'tools.staticfile.filename': indexfile
    }, 
    '/favicon.ico': {
        'tools.staticfile.on': True,
        'tools.staticfile.filename': iconfile
    }
}

cherrypy.config.update({
    'server.socket_host': config.get('Server', 'bind'),
    'server.socket_port': config.getint('Server', 'port')
})

print "Starting Server at http://%s:%d/index.html" % (
    config.get('Server', 'bind'), 
    config.getint('Server', 'port'))
cherrypy.quickstart(JSONServer(), "/", conf)
